﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class musicDatabase : MonoBehaviour
{
	public static musicDatabase Instance;
	public List<musicSample> samples = new List<musicSample>();
	private void Awake()
	{
		musicDatabase.Instance = this;
	}
	public int numSamplesInTrack(int track)
	{
		int num = 0;
		for (int i = 0; i < this.samples.Count; i++)
		{
			if (this.samples[i].allowedTrack == track)
			{
				num++;
			}
		}
		return num;
	}
}
