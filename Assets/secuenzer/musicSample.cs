﻿using System;
using UnityEngine;
[Serializable]
public class musicSample
{
	public string itemName;
	public int allowedTrack;
	public AudioClip musicClip;
}
