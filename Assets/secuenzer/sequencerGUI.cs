﻿using System;
using UnityEngine;
public class sequencerGUI : MonoBehaviour
{
	private sequenzer sequenzerScript;
	public Animator bailaor;
	private bool[] trackIsOn = new bool[sequenzer.maxTracks];
	private bool[] trackOld = new bool[sequenzer.maxTracks];
	private float[] trackVolume = new float[sequenzer.maxTracks];
	private bool showControlGUI;
	private bool showTrackGUI;
	private Vector2 scrollPosition = Vector2.zero;
	private static int topText = 20;
	private static int leftText = 50;
	private static int celWidth = 90;
	private static int celHeight = 30;
	private static int celPadding = 4;
	private bool showAlert;
	private string alertText = string.Empty;
	private bool selectTrack;
	private int trackIndex = -1;
	private int compassIndex = -1;
	private musicSample selectedSample;
	private bool copyToAllTracks;
	private AudioSource tester;
	private void Start()
	{
		this.sequenzerScript = sequenzer.Instance;
		for (int i = 0; i < sequenzer.maxTracks; i++)
		{
			this.trackIsOn[i] = (this.trackOld[i] = true);
		}
		GameObject gameObject = new GameObject("Sample Tester");
		gameObject.transform.parent = base.gameObject.transform;
		gameObject.transform.position = base.gameObject.transform.position;
		this.tester = (gameObject.AddComponent<AudioSource>() as AudioSource);
		this.tester.loop = false;
		this.tester.playOnAwake = false;
		this.showTrackGUI = true;
	}
	private void Update()
	{
		this.setTrackVolume();
	}
	private void OnGUI()
	{
		if (this.showAlert)
		{
			this.showAlertBox();
			return;
		}
		if (this.selectTrack)
		{
			this.showSampleSelection();
			return;
		}
		if (GUI.Button(new Rect(15f, 2f, 150f, 30f), "Make Music") && !this.sequenzerScript.running)
		{
			this.showTrackGUI = !this.showTrackGUI;
			if (this.showTrackGUI)
			{
				this.showControlGUI = false;
			}
		}
		if (GUI.Button(new Rect(180f, 2f, 150f, 30f), "Track Volume"))
		{
			this.showControlGUI = !this.showControlGUI;
			if (this.showControlGUI)
			{
				this.showTrackGUI = false;
			}
		}
		if (this.showTrackGUI)
		{
			this.showTracks();
		}
		if (this.showControlGUI)
		{
			this.showVolume();
		}
		if (!this.sequenzerScript.running)
		{
			//this.bailaor.SetBool("dancing", false);
			if (GUI.Button(new Rect((float)(Screen.width / 2 - 75), (float)(Screen.height - 32), 150f, 30f), "Start"))
			{
				if (this.sequenzerScript.maxCompassFilled() == -1)
				{
					this.alertText = "No music in tracks!";
					this.showAlertBox();
					return;
				}
				this.showTrackGUI = false;
				this.sequenzerScript.nextEventTime = AudioSettings.dspTime + 0.5;
				this.sequenzerScript.finalEventTime = AudioSettings.dspTime + this.sequenzerScript.sampleTime * 1000.0;
				this.sequenzerScript.endBooked = false;
				this.sequenzerScript.running = true;
				//this.bailaor.SetBool("dancing", true);
				Debug.Log("Max Filled=" + this.sequenzerScript.maxCompassFilled());
			}
		}
		else
		{
			if (GUI.Button(new Rect((float)(Screen.width / 2 - 75), (float)(Screen.height - 32), 150f, 30f), "Stop"))
			{
				this.sequenzerScript.stopAllTracks();
			}
		}
	}
	private void showTracks()
	{
		int num = sequencerGUI.leftText + sequencerGUI.celPadding + this.sequenzerScript.maxCompass * sequencerGUI.celWidth;
		int num2 = sequencerGUI.topText + sequencerGUI.celPadding + this.sequenzerScript.numTracks * sequencerGUI.celHeight;
		int num3 = Mathf.Min(Screen.width - 20, num + 20);
		int num4 = Mathf.Min(Screen.height - 80, num2 + 20);
		int num5 = (Screen.width - num3) / 2;
		int num6 = (Screen.height - num4) / 2;
		GUI.Box(new Rect((float)(num5 - 3), (float)(num6 - 3), (float)(num3 + 3), (float)(num4 + 3)), string.Empty);
		this.scrollPosition = GUI.BeginScrollView(new Rect((float)num5, (float)num6, (float)num3, (float)num4), this.scrollPosition, new Rect(0f, 0f, (float)num, (float)num2));
		for (int i = 0; i < this.sequenzerScript.numTracks; i++)
		{
			GUI.Label(new Rect(0f, (float)(sequencerGUI.topText + sequencerGUI.celPadding + i * sequencerGUI.celHeight), (float)sequencerGUI.leftText, (float)sequencerGUI.celHeight), "Track " + (i + 1));
			for (int j = 0; j < this.sequenzerScript.maxCompass; j++)
			{
				if (GUI.Button(new Rect((float)(sequencerGUI.leftText + sequencerGUI.celPadding + j * sequencerGUI.celWidth), (float)(sequencerGUI.topText + sequencerGUI.celPadding + i * sequencerGUI.celHeight), (float)(sequencerGUI.celWidth - sequencerGUI.celPadding), (float)(sequencerGUI.celHeight - sequencerGUI.celPadding)), this.sequenzerScript.tracks[i][j].itemName))
				{
					this.trackIndex = i;
					this.compassIndex = j;
					this.selectedSample = this.sequenzerScript.tracks[i][j];
					this.copyToAllTracks = false;
					this.showSampleSelection();
				}
			}
		}
		for (int k = 0; k < this.sequenzerScript.maxCompass; k++)
		{
			GUI.Label(new Rect((float)(sequencerGUI.leftText + sequencerGUI.celPadding + 10 + k * sequencerGUI.celWidth), 0f, (float)(sequencerGUI.celWidth - sequencerGUI.celPadding), (float)sequencerGUI.topText), "Compass " + (k + 1));
		}
		GUI.EndScrollView();
	}
	private void showVolume()
	{
		int num = 270;
		int num2 = 20 * (this.sequenzerScript.numTracks + 2);
		int num3 = (Screen.width - num) / 2;
		int num4 = (Screen.height - num2) / 2;
		GUI.Box(new Rect((float)(num3 - 10), (float)(num4 - 10), (float)(num + 20), (float)(num2 + 20)), string.Empty);
		for (int i = 0; i < this.sequenzerScript.numTracks; i++)
		{
			this.trackIsOn[i] = GUI.Toggle(new Rect((float)num3, (float)(num4 + i * 30), 90f, 20f), this.trackIsOn[i], "Track " + (i + 1));
			this.sequenzerScript.trackBuffer[i].volume = GUI.HorizontalSlider(new Rect((float)(num3 + 100), (float)(num4 + i * 30), 170f, 20f), this.sequenzerScript.trackBuffer[i].volume, 0f, 1f);
			this.sequenzerScript.trackBuffer[i + this.sequenzerScript.numTracks].volume = this.sequenzerScript.trackBuffer[i].volume;
		}
	}
	private void setTrackVolume()
	{
		for (int i = 0; i < this.sequenzerScript.numTracks; i++)
		{
			if (this.trackIsOn[i] != this.trackOld[i])
			{
				this.sequenzerScript.trackBuffer[i].volume = !this.trackIsOn[i] ? 0f : 1f;
				this.sequenzerScript.trackBuffer[i + this.sequenzerScript.numTracks].volume =!this.trackIsOn[i] ? 0f : 1f;
				this.trackOld[i] = this.trackIsOn[i];
			}
		}
	}
	private void showAlertBox()
	{
		this.showAlert = true;
		int num = 270;
		int num2 = 120;
		int num3 = (Screen.width - num) / 2;
		int num4 = (Screen.height - num2) / 2;
		GUI.Box(new Rect((float)num3, (float)num4, (float)num, (float)num2), string.Empty);
		GUI.Label(new Rect((float)(num3 + 10), (float)(num4 + 10), (float)(num - 20), 20f), this.alertText);
		if (GUI.Button(new Rect((float)(num3 + 70), (float)(num4 + 50), 130f, 30f), "Ok"))
		{
			this.showAlert = false;
		}
	}
	private void showSampleSelection()
	{
		this.selectTrack = true;
		int num = 400;
		int num2 = 230;
		int num3 = (Screen.width - num) / 2;
		int num4 = (Screen.height - num2) / 2;
		int num5 = 120;
		int num6 = (num - 2 * num5) / 3;
		GUI.Box(new Rect((float)num3, (float)num4, (float)num, (float)num2), string.Empty);
		GUI.Label(new Rect((float)(num3 + 10), (float)(num4 + 5), (float)(num - 20), 22f), "Click to listen and select. Then 'Ok' to asign to compass.");
		musicDatabase instance = musicDatabase.Instance;
		int num7 = instance.numSamplesInTrack(this.trackIndex) + 1;
		int num8 = sequencerGUI.celPadding + num7 * sequencerGUI.celWidth;
		int num9 = sequencerGUI.celPadding + sequencerGUI.celHeight;
		int num10 = Mathf.Min(num - 20, num8 + 20);
		int num11 = Mathf.Min(num2 - 20, num9 + 20);
		int num12 = num3 + 10;
		int num13 = num4 + 35;
		GUI.Box(new Rect((float)(num12 - 5), (float)(num13 - 5), (float)(num10 + 10), (float)(num11 + 10)), string.Empty);
		this.scrollPosition = GUI.BeginScrollView(new Rect((float)num12, (float)num13, (float)num10, (float)num11), this.scrollPosition, new Rect(0f, 0f, (float)num8, (float)num9));
		int num14 = 1;
		if (GUI.Button(new Rect((float)sequencerGUI.celPadding, (float)sequencerGUI.celPadding, (float)(sequencerGUI.celWidth - sequencerGUI.celPadding), (float)(sequencerGUI.celHeight - sequencerGUI.celPadding)), this.sequenzerScript.blankClip.itemName))
		{
			this.selectedSample = this.sequenzerScript.blankClip;
		}
		for (int i = 0; i < instance.samples.Count; i++)
		{
			if (instance.samples[i].allowedTrack == this.trackIndex)
			{
				if (GUI.Button(new Rect((float)(sequencerGUI.celPadding + num14 * sequencerGUI.celWidth), (float)sequencerGUI.celPadding, (float)(sequencerGUI.celWidth - sequencerGUI.celPadding), (float)(sequencerGUI.celHeight - sequencerGUI.celPadding)), instance.samples[i].itemName))
				{
					this.selectedSample = instance.samples[i];
					this.tester.clip = this.selectedSample.musicClip;
					this.tester.Play();
				}
				num14++;
			}
		}
		GUI.EndScrollView();
		GUI.Label(new Rect((float)(num3 + 10), (float)(num13 + num11 + 15), (float)(num - 20), 22f), "Currently selected sample: " + this.selectedSample.itemName);
		this.copyToAllTracks = GUI.Toggle(new Rect((float)(num3 + 10), (float)(num13 + num11 + 45), (float)(num - 20), 22f), this.copyToAllTracks, "Copy to ALL compass in the track");
		if (GUI.Button(new Rect((float)(num3 + num6), (float)(num4 + num2 - 50), (float)num5, 30f), "Ok"))
		{
			if (this.tester.isPlaying)
			{
				this.tester.Stop();
			}
			if (this.copyToAllTracks)
			{
				for (int j = 0; j < this.sequenzerScript.maxCompass; j++)
				{
					this.sequenzerScript.tracks[this.trackIndex][j] = this.selectedSample;
				}
			}
			else
			{
				this.sequenzerScript.tracks[this.trackIndex][this.compassIndex] = this.selectedSample;
			}
			this.selectTrack = false;
		}
		if (GUI.Button(new Rect((float)(num3 + num5 + 2 * num6), (float)(num4 + num2 - 50), (float)num5, 30f), "Cancel"))
		{
			if (this.tester.isPlaying)
			{
				this.tester.Stop();
			}
			this.selectTrack = false;
		}
	}
}
