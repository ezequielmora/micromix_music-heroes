﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class sequenzer : MonoBehaviour
{
	public enum sampleLength
	{
		Number_of_frames,
		Take_from_clip,
		Length_in_seconds
	}
	public static sequenzer Instance;
	public sequenzer.sampleLength sampleLengthBy;
	public int numFrames;
	public AudioClip sampleToken;
	public float seconds;
	public int sampleRate = 44100;
	public musicSample blankClip;
	private int numChannels = 2;
	public static int maxTracks = 5;
	[Range(1f, 5f)]
	public int numTracks;
	[Range(1f, 30f)]
	public int maxCompass = 10;
	public List<List<musicSample>> tracks = new List<List<musicSample>>();
	public AudioSource[] trackBuffer = new AudioSource[10];
	private int currentCompass;
	private int flip;
	public double nextEventTime;
	public double finalEventTime;
	public double sampleTime;
	private double deltaTime;
	public bool endBooked;
	public bool running;
	private void Awake()
	{
		sequenzer.Instance = this;
	}
	private void Start()
	{
		this.sampleTime = (double)this.numFrames / (double)this.sampleRate;
		this.deltaTime = this.sampleTime / 2.0;
		this.blankClip = new musicSample();
		this.blankClip.allowedTrack = -1;
		this.blankClip.itemName = "Empty";
		float[] array = new float[this.numFrames * this.numChannels];
		for (int i = 0; i < this.numFrames; i++)
		{
			array[i] = 0f;
		}
		this.blankClip.musicClip = AudioClip.Create("Blank", this.numFrames, this.numChannels, this.sampleRate, false, false);
		this.blankClip.musicClip.SetData(array, 0);
		this.instantiateAudioObjects();
		this.loadTracksClip(this.currentCompass);
	}
	private void Update()
	{
		if (!this.running)
		{
			return;
		}
		double dspTime = AudioSettings.dspTime;
		if (dspTime >= this.finalEventTime)
		{
			this.running = false;
			this.currentCompass = 0;
			return;
		}
		if (dspTime + this.deltaTime > this.nextEventTime)
		{
			this.loadTracksClip(this.currentCompass, this.nextEventTime);
			if (this.currentCompass > this.maxCompassFilled() && !this.endBooked)
			{
				this.finalEventTime = this.nextEventTime;
				this.endBooked = true;
			}
			this.currentCompass++;
			this.currentCompass %= this.maxCompass;
			this.nextEventTime += this.sampleTime;
		}
	}
	public void stopAllTracks()
	{
		this.running = false;
		this.finalEventTime = 0.0;
		for (int i = 0; i < this.trackBuffer.Length; i++)
		{
			if (!(this.trackBuffer[i] == null))
			{
				if (this.trackBuffer[i].isPlaying)
				{
					this.trackBuffer[i].Stop();
				}
			}
		}
		this.currentCompass = 0;
	}
	private void loadTracksClip(int whichCompas, double nextEvent)
	{
		for (int i = 0; i < this.numTracks; i++)
		{
			if (whichCompas >= this.tracks[i].Count)
			{
				this.trackBuffer[i + this.flip * this.numTracks].clip = this.blankClip.musicClip;


			}
			else
			{
				if (this.tracks[i][whichCompas] == null)
				{
					this.trackBuffer[i + this.flip * this.numTracks].clip = this.blankClip.musicClip;
				}
				else
				{
					this.trackBuffer[i + this.flip * this.numTracks].clip = this.tracks[i][whichCompas].musicClip;
				}
			}
			this.trackBuffer[i + this.flip * this.numTracks].PlayScheduled(nextEvent);
		}
		this.flip = 1 - this.flip;
	}
	private void loadTracksClip(int whichCompas)
	{
		for (int i = 0; i < this.numTracks; i++)
		{
			if (whichCompas >= this.tracks[i].Count)
			{
				this.trackBuffer[i + this.flip * this.numTracks].clip = this.blankClip.musicClip;
			}
			else
			{
				if (this.tracks[i][whichCompas] == null)
				{
					this.trackBuffer[i + this.flip * this.numTracks].clip = this.blankClip.musicClip;
				}
				else
				{
					this.trackBuffer[i + this.flip * this.numTracks].clip = this.tracks[i][whichCompas].musicClip;
				}
			}
		}
		this.flip = 1 - this.flip;
	}
	private void instantiateAudioObjects()
	{
		for (int i = 0; i < this.numTracks; i++)
		{
			List<musicSample> list = new List<musicSample>();
			for (int j = 0; j < this.maxCompass; j++)
			{
				list.Add(this.blankClip);
			}
			this.tracks.Add(list);
		}
		for (int k = 0; k < 2 * this.numTracks; k++)
		//for (int k = 0; k < this.numTracks; k++)

		{
			GameObject gameObject;
			if (k < this.numTracks)
			{
				gameObject = new GameObject("Track " + k + " primary");
			}
			else
			{
				gameObject = new GameObject("Track " + k + " secondary");
			}
			gameObject.transform.parent = base.gameObject.transform;
			gameObject.transform.position = base.gameObject.transform.position;
			this.trackBuffer[k] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.trackBuffer[k].loop = false;
			this.trackBuffer[k].playOnAwake = false;
		}
	}
	public int maxCompassFilled()
	{
		int num = -1;
		for (int i = 0; i < this.numTracks; i++)
		{
			for (int j = 0; j < this.maxCompass; j++)
			{
				if (this.tracks[i][j] != this.blankClip && j > num)
				{
					num = j;
				}
			}
		}
		return num;
	}
}
