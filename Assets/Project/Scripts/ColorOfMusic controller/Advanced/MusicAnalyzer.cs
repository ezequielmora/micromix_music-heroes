﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]

public class MusicAnalyzer : MonoBehaviour
{
    // Graphics
    private float bass;
    private float voice;
    private float red;
    private float green;
    private float blue;
    public float multiplyBass = 0.86f;
    public float multiplyVoice = 0.8f;
    public float multiplyRed = 8.5f / 7;
    public float multiplyGreen = 1.7f / 5;
    public float multiplyBlue = 2.7f / 5;
    [SerializeField]
    private Color color = Color.white;

    // Music 
    [SerializeField]
    private bool isMain = false;
    public static List<MusicAnalyzer> instances = null;
    private AudioSource audio;
    public bool isLoopingList = true;
    private int musicCounter = 0;
    public List<AudioClip> musicList;
    public bool isRandom = true;
    // Spectrum is multiplayed with volume. To take any values from spectrum, volume should have not zero value
    float[] spectrum = new float[1024];
    private float spectrumMultiply = 1;
    public FFTWindow window = FFTWindow.BlackmanHarris;
    private float minVolume = 0.0005f;

    void Awake()
    {
        if(instances == null)
        {
            instances = new List<MusicAnalyzer>();
        }
        if (isMain && musicList.Count > 0)
        {
            instances.Insert(0,this);
        } else
        {
            instances.Add(this);
        }
    }

    // Use this for initialization
    void Start () {
        if (instances[0] == this)
        {
            instances[0].isMain = true;
            for (int i = 1; i < instances.Count; i++)
            {
                instances[i].isMain = false;
            }
        }
        audio = GetComponent<AudioSource>();
        if (musicList.Count > 1)
        {
            audio.clip = isRandom ? musicList[Random.Range(0, musicList.Count)] : musicList[musicCounter++];
            audio.Play();
            audio.loop = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(isMain && musicList.Count > 1 && (audio.clip.length-0.01) <= audio.time)
        {
            NextSoung();
        }
        AnalyzeMusic();
        UpdateColor();
    }

    void NextSoung()
    {
        if (isLoopingList || musicCounter < musicList.Count)
        {
            audio.clip = isRandom ? musicList[Random.Range(0, musicList.Count)] : musicList[musicCounter++];
            audio.Play();
            if (musicCounter >= musicList.Count)
            {
                musicCounter = 0;
            }
        }
    }

    void AnalyzeMusic()
    {
        if (AudioListener.volume < minVolume)
        {
            AudioListener.volume = minVolume;
        };

        if (audio.volume < minVolume)
        {
            audio.volume = minVolume;
        };
        audio.GetSpectrumData(spectrum, 0, window);
        spectrumMultiply = 1 / AudioListener.volume + 1 / audio.volume;
    }

    void UpdateColor()
    {
        for (int i = 1; i < 6; i++)
        {
            bass += spectrum[i];
        }
        bass *= multiplyBass * spectrumMultiply;

        voice = 0;
        for (int i = 77; i < 103; i++)
        {
            voice += spectrum[i];
        }
        voice *= multiplyVoice * spectrumMultiply;

        red = 0;
        for (int i = 10; i < 27; i++)
        {
            red += spectrum[i];
        }
        red *= multiplyRed * spectrumMultiply;

        green = 0;
        for (int i = 51; i < 257; i++)
        {
            green += spectrum[i];
        }
        green *= multiplyGreen * spectrumMultiply;

        blue = 0;
        for (int i = 358; i < 1024; i++)
        {
            blue += spectrum[i];
        }
        blue *= multiplyBlue * spectrumMultiply;

        color = new Color(red, green, blue, bass);
    }
    
    public float GetBass() {
        if (bass > 1)
        {
            bass = 1;
        } else if (bass < 0)
        {
            bass = 0;
        }
        return bass;
    }

    public float GetVoice()
    {
        if (voice > 1)
        {
            voice = 1;
        }
        else if (voice < 0)
        {
            voice = 0;
        }
        return voice;
    }

    public float GetRed()
    {
        if (red > 1)
        {
            red = 1;
        }
        else if (red < 0)
        {
            red = 0;
        }
        return red;
    }

    public float GetGreen()
    {
        if (green > 1)
        {
            green = 1;
        }
        else if (green < 0)
        {
            green = 0;
        }
        return green;
    }

    public float GetBlue()
    {
        if (blue > 1)
        {
            blue = 1;
        }
        else if (blue < 0)
        {
            blue = 0;
        }
        return blue;
    }

}
