﻿using UnityEngine;
using UnityEngine.UI;

// Start namespace ColorGetter
namespace ColorGetter
{

    // Start of define enums
    public enum setColorAt
    {
        none,
        material,
        sprite,
        light,
        background,
        image,
        rawImage,
        particleMaterial
    }

    public enum setAsTransparencyValue
    {
        bassValue,
        voiceValue,
        warmValue,
        middleValue,
        coldValue,
        userTransparencyValue,
        zero,
        one
    }

    public enum setAsAdditionalValue
    {
        voiceValue,
        bassValue,
        warmValue,
        middleValue,
        coldValue,
        userAdditionalValue,
        zero
    }

    public enum setAsRemovalValue
    {
        voiceValue,
        bassValue,
        warmValue,
        middleValue,
        coldValue,
        userAdditionalValue,
        zero,
    }
    
    public enum setAsColor
    {
        middleValue,
        warmValue,
        coldValue,
        bassValue,
        voiceValue,
        userColorValue,
        zero,
        one
    }
    
    // End of define enums

    public class GetAudioCreateColor : MonoBehaviour
    {
        [SerializeField]
        private Color color;
        [SerializeField]
        private bool isColorBlending = true;
        [SerializeField]
        private float speedOfBlending = 0.1f;
        [SerializeField]
        private setColorAt canvas = setColorAt.none;
        public bool isHSV = false;
        [SerializeField]
        private MusicAnalyzer musicAnalyzer;
        // isEmissive is used to material component only
        [SerializeField]
        private bool isEmissive = false;
        // powerOfLight is multiply to transparency value and set as intensity
        [SerializeField]
        private float powerOfLite = 1;
        [SerializeField]
        private Material particleMaterial;
        // Please chose good emun in option
        public Color userColor = Color.black;
        public Color baseColor = Color.black;
        public Color removeColor = Color.black;
        public bool invertRed;
        public bool invertGreen;
        public bool invertBlue;
        public bool invertTransparency;

        [SerializeField]
        private setAsColor setAsRedColor = setAsColor.warmValue;
        [SerializeField]
        private setAsColor setAsGreenColor = setAsColor.middleValue;
        [SerializeField]
        private setAsColor setAsBlueColor = setAsColor.coldValue;
        [SerializeField]
        private setAsColor setAsTransparencyColor = setAsColor.bassValue;
        
        [Range(0, 1)]
        private float red;
        [Range(0, 1)]
        private float green;
        [Range(0, 1)]
        private float blue;
        [Range(0, 1)]
        private float transparency;
        [Range(0, 1)]
        private float oldRed;
        [Range(0, 1)]
        private float oldGreen;
        [Range(0, 1)]
        private float oldBlue;
        [Range(0, 1)]
        private float oldTransparency;

        // Use this for initialization
        void Start()
        {
            if(musicAnalyzer == null)
            {
                musicAnalyzer = MusicAnalyzer.instances[0];
            }

        }

        // Update is called once per frame
        void Update()
        {
            if(!isHSV)
            {
                setColor();
            } else
            {
                setHSVColor();
            }
            switch (canvas)
            {
                case setColorAt.material:
                    SetMaterial();
                    break;
                case setColorAt.sprite:
                    SetSprite();
                    break;
                case setColorAt.light:
                    SetLight();
                    break;
                case setColorAt.background:
                    SetBackground();
                    break;
                case setColorAt.image:
                    SetImage();
                    break;
                case setColorAt.rawImage:
                    SetRawImage();
                    break;
                case setColorAt.particleMaterial:
                    SetParticleMaterial();
                    break;
                case setColorAt.none:
                default:
                    break;
            }
        }

        public Color GetColor()
        {
            return color;
        }

        void setColor()
        {
            CalculateRed();
            CalculateGreen();
            CalculateBlue();
            CalculateTransparency();
            color = new Color( red, green, blue, transparency);
        }
        
        void setHSVColor()
        {
            CalculateRed();
            CalculateGreen();
            CalculateBlue();
            CalculateTransparency();
            float h = (red + green + blue) % 1.0f;
            float s = baseColor.r + baseColor.g + baseColor.b;
            float v = 1 - removeColor.r - removeColor.g - removeColor.b;
            Color newColor = Color.HSVToRGB(h, s, v);
            newColor.a = color.a;
            color = Color.Lerp(color, newColor, speedOfBlending);
        }

        void CalculateRed()
            {
            switch(setAsRedColor)
            {
                case setAsColor.bassValue:
                    red = musicAnalyzer.GetBass();
                    break;
                case setAsColor.voiceValue:
                    red = musicAnalyzer.GetVoice();
                    break;
                case setAsColor.coldValue:
                    red = musicAnalyzer.GetBlue();
                    break;
                case setAsColor.middleValue:
                    red = musicAnalyzer.GetGreen();
                    break;
                default:
                case setAsColor.warmValue:
                    red = musicAnalyzer.GetRed();
                    break;
                case setAsColor.userColorValue:
                    red = userColor.r;
                    break;
                case setAsColor.one:
                    red = 1;
                    break;
                case setAsColor.zero:
                    red = 0;
                    break;
            }
            red += baseColor.r;
            red -= removeColor.r;
            red = invertRed ? 1 - red : red;
            if(isColorBlending)
            {
                red = Mathf.Lerp(red, oldRed, speedOfBlending);
                oldRed = red;
            }
        }

        void CalculateGreen()
            {
            switch (setAsGreenColor)
            {
                case setAsColor.bassValue:
                    green = musicAnalyzer.GetBass();
                    break;
                case setAsColor.voiceValue:
                    green = musicAnalyzer.GetVoice();
                    break;
                case setAsColor.coldValue:
                    green = musicAnalyzer.GetBlue();
                    break;
                default:
                case setAsColor.middleValue:
                    green = musicAnalyzer.GetGreen();
                    break;
                case setAsColor.warmValue:
                    green = musicAnalyzer.GetRed();
                    break;
                case setAsColor.userColorValue:
                    green = userColor.g;
                    break;
                case setAsColor.one:
                    green = 1;
                    break;
                case setAsColor.zero:
                    green = 0;
                    break;
            }
            green += baseColor.g;
            green -= removeColor.g;
            green = invertGreen ? 1 - green : green;
            if (isColorBlending)
            {
                green = Mathf.Lerp(green, oldGreen, speedOfBlending);
                oldGreen = green;
            }
        }

        void CalculateBlue()
            {
            switch (setAsBlueColor)
            {
                case setAsColor.bassValue:
                    blue = musicAnalyzer.GetBass();
                    break;
                case setAsColor.voiceValue:
                    blue = musicAnalyzer.GetVoice();
                    break;
                default:
                case setAsColor.coldValue:
                    blue = musicAnalyzer.GetBlue();
                    break;
                case setAsColor.middleValue:
                    blue = musicAnalyzer.GetGreen();
                    break;
                case setAsColor.warmValue:
                    blue = musicAnalyzer.GetRed();
                    break;
                case setAsColor.userColorValue:
                    blue = userColor.b;
                    break;
                case setAsColor.one:
                    blue = 1;
                    break;
                case setAsColor.zero:
                    blue = 0;
                    break;
            }
            blue += baseColor.b;
            blue -= removeColor.b;
            blue = invertBlue ? 1 - blue : blue;
            if (isColorBlending)
            {
                blue = Mathf.Lerp(blue, oldBlue, speedOfBlending);
                oldBlue = blue;
            }
        }

        void CalculateTransparency()
        {
            switch (setAsTransparencyColor)
            {
                case setAsColor.bassValue:
                    transparency = musicAnalyzer.GetBass();
                    break;
                case setAsColor.voiceValue:
                    transparency = musicAnalyzer.GetVoice();
                    break;
                case setAsColor.coldValue:
                    transparency = musicAnalyzer.GetBlue();
                    break;
                case setAsColor.middleValue:
                    transparency = musicAnalyzer.GetGreen();
                    break;
                case setAsColor.warmValue:
                    transparency = musicAnalyzer.GetRed();
                    break;
                case setAsColor.userColorValue:
                    transparency = userColor.a;
                    break;
                default:
                case setAsColor.one:
                    transparency = 1;
                    break;
                case setAsColor.zero:
                    transparency = 0;
                    break;
            }
            transparency += baseColor.a;
            transparency -= removeColor.a;
            transparency = invertTransparency ? 1 - transparency : transparency;
            if (isColorBlending)
            {
                transparency = Mathf.Lerp(transparency, oldTransparency, speedOfBlending);
                oldTransparency = transparency;
            }
        }

        void SetMaterial()
        {
            if(GetComponent<MeshRenderer>() == null)
            {
                Debug.Log("Probably you dont have material component in object " + this.gameObject.name);
            }
            else
            {
                GetComponent<MeshRenderer>().material.color = color;
                if (isEmissive)
                {
                    GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", color);
                } else
                {
                    GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
                }
            }
        }

        void SetSprite()
        {
            if (GetComponent<SpriteRenderer>() == null)
            {
                Debug.Log("Probably you dont have sprite renderer component in object " + this.gameObject.name);
            }
            else
            {
                GetComponent<SpriteRenderer>().color = color;
            }
        }

        void SetLight()
        {
            if (GetComponent<Light>() == null)
            {
                Debug.Log("Probably you dont have light component in object " + this.gameObject.name);
            }
            else
            {
                GetComponent<Light>().color = color;
                GetComponent<Light>().intensity = transparency * powerOfLite;
            }
        }

        void SetBackground()
        {
            if (GetComponent<Camera>() == null)
            {
                Debug.Log("Probably you dont have camera component in object " + this.gameObject.name);
            } else
            {
                GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
                GetComponent<Camera>().backgroundColor = color;
            }
        }

        void SetImage()
        {
            if (GetComponent<Image>() == null)
            {
                Debug.Log("Probably you dont have image component in object " + this.gameObject.name);
            }
            else
            {
                GetComponent<Image>().color = color;
            }
        }

        void SetRawImage()
        {
            if (GetComponent<RawImage>() == null)
            {
                Debug.Log("Probably you dont have raw image component in object " + this.gameObject.name);
            }
            else
            {
                GetComponent<RawImage>().color = color;
            }
        }

        void SetParticleMaterial()
        {
            if (particleMaterial == null)
            {
                Debug.Log("Probably you dont have particleMaterial component in object " + this.gameObject.name);
            }
            else
            {
                particleMaterial.color = color;
                if (isEmissive)
                {
                    particleMaterial.SetColor("_EmissionColor", color);
                } else
                {
                    particleMaterial.SetColor("_EmissionColor", Color.black);
                }
            }
        }
    }

    // End of namespace ColorGetter
}