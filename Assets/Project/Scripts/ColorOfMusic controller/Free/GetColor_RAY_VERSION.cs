﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Material))]

public class GetColor_RAY_VERSION : MonoBehaviour {
    // AEGIS FIX EXPONGO VARIABLE
    public MusicAnalyzerFree analyzer;

    private Material material;
    public Color basicsColor = Color.grey;
    private Color color;
    private Color newColor;
    public bool isEmissive = false;
    public bool useRed = true;
    public bool useGreen = true;
    public bool useBlue = true;

    // Use this for initialization
    void Start () {
        // AEGIS FIX - MOVE
        //analyzer = MusicAnalyzerFree.instance;
        material = GetComponent<Renderer>().material;
        //material.color = basicsColor;
        material.SetColor("_Color", basicsColor);
    }

    // Update is called once per frame
    void Update() {

        // AEGIS FIX MOVE TO UPDATE

        if (GameObject.FindGameObjectWithTag("SC") != null)
        { 

            analyzer = GameObject.FindGameObjectWithTag("SC").GetComponent<MusicAnalyzerFree>();   //MusicAnalyzerFree.instance;

        }


        if (analyzer != null)
        {
            GetColorUpdate();
        }
        
        if(isEmissive)
        {
            GetEmissiveColorUpdate();
        } else
        {
            GetNoEmissiveColorUpdate();
        }
	}

    void GetColorUpdate()
    {


            newColor = analyzer.GetColor() + basicsColor;
            if (useRed && newColor.r > 1)
            {
                newColor.r = 1;
            }
            else if (useRed && newColor.r > 1)
            {
                newColor.r = 0;
            }
            if (useGreen && newColor.g > 1)
            {
                newColor.g = 1;
            }
            else if (useGreen && newColor.g < 0)
            {
                newColor.g = 0;
            }
            if (useBlue && newColor.r > 1)
            {
                newColor.b = 1;
            }
            else if (useBlue && newColor.b < 0)
            {
                newColor.b = 0;
            }

            color = new Color(useRed ? newColor.r : color.r, useGreen ? newColor.g : color.g, useBlue ? newColor.b : color.b);

            //material.color = color;

            material.SetColor("_Color", color);


            //material.SetColor("_RightBottomColor", color.gamma);







    }

    void GetEmissiveColorUpdate()
    {
        //material.SetColor("_EmissionColor", color);
        material.SetColor("_Color", color);


    }

    void GetNoEmissiveColorUpdate()
    {
        //material.SetColor("_EmissionColor", Color.black);
        material.SetColor("_Color", Color.black);

    }

}
