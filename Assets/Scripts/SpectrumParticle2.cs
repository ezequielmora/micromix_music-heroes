﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class SpectrumParticle2 : MonoBehaviour {

	private float angle = 0;
	private  ParticleSystem particles;
	private WaveForm1 waveForm;
	private int m = 0;
	private float[] averagePeakSamples = {1f,1f,1f,1f,1f,1f,1f,1f,1f,1f};
	private float averagePeak = 1f;
	private int averagePeakInt = 1;
	private int peakModuIndex = 0;

	
	public int sampleCount = 512;
	public float averagePeakScalar = 0.7f;
	public float sampleScalar = 1f;
	public float angleScalar = 2f;
	public float averagePeakIntMutator = 80f;
	public float particleSizeScalar = 1f;
	
	public void Create(WaveForm1 waveFormRef ){
		waveForm = waveFormRef;		
		particles = gameObject.GetComponent<ParticleSystem>();
		
	}
	
	public void UpdateAudioSamples(){
		
		
		
		int i;
		int j = 0;

		ParticleSystem.Particle[] _particles = new ParticleSystem.Particle[particles.particleCount];
		particles.GetParticles(_particles);

		averagePeakSamples [peakModuIndex] = 0;		

		int k = m%averagePeakInt;
		if(k%2 == 0){
			k*= -1;
		}
		float kk = (float)k;
		for (i=1; i<sampleCount; i++) {
			float s = waveForm.samples[i]*waveForm.sample1ScalarGlobal;
			float ss = s*sampleScalar;
						
			averagePeakSamples[peakModuIndex] = Math.Max(averagePeakSamples[peakModuIndex],ss);

			if(j<_particles.Length){
				if(j<(sampleCount-1)){
					
					if(ss>averagePeak||ss<(averagePeak*-1)){
						continue;						
					}
					

					angle += Mathf.Abs(s*((angleScalar*kk)));
					if(angle>360f){
						angle -=360; 
					}

					Vector3 pos = new Vector3();
					pos.x = ((kk)*0.5f)*Mathf.Sin(Mathf.Deg2Rad*(angle*kk));
					pos.y = ((kk)*0.5f)* Mathf.Cos(Mathf.Deg2Rad*(angle*kk));

					_particles[j].position = pos;
					_particles[j].size = ss*kk*particleSizeScalar;

				}
			}
			
			j++;
			
		}
				
		particles.SetParticles(_particles, _particles.Length);
		
		peakModuIndex++;
		if (peakModuIndex == 10) {
			peakModuIndex = 0;		
		}
		averagePeak = 0;
		for (i=0; i<10; i++) {
			averagePeak += averagePeakSamples[i];
		}
		averagePeak /= 10f;
		
		averagePeak *= averagePeakScalar;
		
		averagePeakInt  = (Mathf.FloorToInt(averagePeak*averagePeakIntMutator)+1);
		
		
		
		m++;
		if (m > averagePeakInt) {
			m = 0;	

		}
		
	}
}
