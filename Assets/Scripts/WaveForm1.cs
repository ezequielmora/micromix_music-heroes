﻿using UnityEngine;
using System.Collections;

public class WaveForm1 : MonoBehaviour {

	public AudioClip song;
	[System.NonSerialized]
	public float[] samples;
	[System.NonSerialized]
	public float[] samples2;
	public int sampleCount = 512;

	public SpectrumParticle1 particle1;
	public SpectrumParticle2 particle2;
	public SpectrumParticle3 particle3;

	[System.NonSerialized]
	public float sample1ScalarGlobal = 1f;
	[System.NonSerialized]
	public float sample1ScalarGlobalMax = 2f;

	[System.NonSerialized]
	public float sample2ScalarGlobal = 1f;
	[System.NonSerialized]
	public float sample2ScalarGlobalMax = 2f;

	public void Start(){

		samples = new float[sampleCount];
		samples2 = new float[sampleCount];

		particle1.Create (this);
		particle2.Create (this);
		particle3.Create (this);

		GetComponent<AudioSource>().clip = song;
		GetComponent<AudioSource>().Play ();
	}

	public void Update(){

		GetComponent<AudioSource>().GetOutputData (samples, 0);
		GetComponent<AudioSource>().GetSpectrumData (samples2, 0, FFTWindow.BlackmanHarris);

		particle1.UpdateAudioSamples ();
		particle2.UpdateAudioSamples ();
		particle3.UpdateAudioSamples ();
	}
}
