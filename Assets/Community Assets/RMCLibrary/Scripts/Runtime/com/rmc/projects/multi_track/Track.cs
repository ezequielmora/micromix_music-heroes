/**
* Copyright (C) 2005-2014 by Rivello Multimedia Consulting (RMC).                    
* code [at] RivelloMultimediaConsulting [dot] com                                                  
*                                                                      
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the      
* "Software"), to deal in the Software without restriction, including  
* without limitation the rights to use, copy, modify, merge, publish,  
* distribute, sublicense, and#or sell copies of the Software, and to   
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:                                            
*                                                                      
* The above copyright notice and this permission notice shall be       
* included in all copies or substantial portions of the Software.      
*                                                                      
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR    
* OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.                                      
*/
// Marks the right margin of code *******************************************************************


//--------------------------------------
//  Imports
//--------------------------------------
using UnityEngine;


//--------------------------------------
//  Namespace
//--------------------------------------
using System.Collections;
using com.rmc.exceptions;


namespace com.rmc.projects.multi_track
{
	
	//--------------------------------------
	//  Namespace Properties
	//--------------------------------------
	/// <summary>
	/// Track transition mode.
	/// </summary>
	public enum TrackTransitionMode
	{
		NULL,
		FADE_TO_PLAY_BEGIN,
		FADE_TO_PLAY_PROGRESS,
		FADE_TO_PLAY_COMPLETE,
		FADE_TO_STOP_BEGIN,
		FADE_TO_STOP_PROGRESS,
		FADE_TO_STOP_COMPLETE

	}
	
	//--------------------------------------
	//  Class Attributes
	//--------------------------------------
	
	
	//--------------------------------------
	//  Class
	//--------------------------------------
	public class Track 
	{

		//--------------------------------------
		//  Properties
		//--------------------------------------
		
		// GETTER / SETTER
		
		// PUBLIC
		/// <summary>
		/// The audio clip.
		/// </summary>
		private AudioClip _audioClip;
		public AudioClip audioClip
		{
			get {
				return _audioClip;
			}

		}

		
		/// <summary>
		/// The isPlaying status
		/// </summary>
		private bool _isPlaying_boolean;
		public bool isPlaying
		{
			get 
			{
				return _isPlaying_boolean;

			}
		}


		/// <summary>
		/// The _minimum intensity_float.
		/// </summary>
		private float _minimumIntensity_float;
		public float minimumIntensity
		{
			get {
				return _minimumIntensity_float;
			}
			
		}

		
		/// <summary>
		/// The _maximum intensity_float.
		/// </summary>
		private float _maximumIntensity_float;
		public float maximumIntensity
		{
			get {
				return _maximumIntensity_float;
			}
			
		}



		/// <summary>
		/// DETERMINE IF VALID INTENSITY
		/// </summary>
		public bool isValidIntensity (float aIntensity_float)
		{
			bool isValid_boolean = false;
			if (aIntensity_float >= _minimumIntensity_float &&
			    aIntensity_float <= _maximumIntensity_float) {
				isValid_boolean = true;
			}

			return isValid_boolean;
		}
		
		
		// PUBLIC STATIC
		
		// PRIVATE
		private TrackTransitionMode __trackTransitionMode; 
		private TrackTransitionMode _trackTransitionMode
		{
			set {
				__trackTransitionMode = value;
				if (_parentMultiTrack != null) {
					//Debug.Log ("m: " + __trackTransitionMode + " p: " + _isPlaying_boolean + " v: " +  _parentMultiTrack._getBestAudioSourceForTrack (this).volume.ToString ("00.0") );
				}
				switch (__trackTransitionMode) {


				/*
				 * 
				 * WE STAY HERE: NO ACTIVE STATE
				 * 
				 **/
				case TrackTransitionMode.NULL:
					break;


					
				/*
				 * 
				 * WE STAY HERE: PLAYING
				 * 
				 **/
				case TrackTransitionMode.FADE_TO_PLAY_BEGIN:
					_isPlaying_boolean = true;
					_doFadeAudioSourceFromStopToPlay();
					_parentMultiTrack._audioParentComponent.StartCoroutineDelegate ( onFadeToPlayComplete_Coroutine); 
					_trackTransitionMode = TrackTransitionMode.FADE_TO_PLAY_PROGRESS;
					break;
				case TrackTransitionMode.FADE_TO_PLAY_PROGRESS:
					_parentMultiTrack._audioParentComponent.StartCoroutine (onShowProgress());
					break;
				case TrackTransitionMode.FADE_TO_PLAY_COMPLETE:
					_trackTransitionMode = TrackTransitionMode.NULL;
					break;


				/*
				 * 
				 * WE STAY HERE: STOPING
				 * 
				 **/
				case TrackTransitionMode.FADE_TO_STOP_BEGIN:
					_isPlaying_boolean = false;
					_doFadeAudioSourceFromPlayToStop();
					_parentMultiTrack._audioParentComponent.StartCoroutineDelegate ( onFadeToStopComplete_Coroutine); 
					_trackTransitionMode = TrackTransitionMode.FADE_TO_STOP_PROGRESS;
					break;
				case TrackTransitionMode.FADE_TO_STOP_PROGRESS:
					_parentMultiTrack._audioParentComponent.StartCoroutine (onShowProgress());
					break;
				case TrackTransitionMode.FADE_TO_STOP_COMPLETE:
					_parentMultiTrack._audioParentComponent.StopCoroutine ("onShowProgress");
					_parentMultiTrack._getBestAudioSourceForTrack (this).Stop();
					_trackTransitionMode = TrackTransitionMode.NULL;
					break;


				/*
				 * 
				 * WE STAY HERE: 100%, THIS NEVER WILL HAPPEN
				 * 
				 **/
				default:
					#pragma warning disable 0162
					throw new SwitchStatementException();
					break;
					#pragma warning restore 0162
				}
			}
			get {
				return __trackTransitionMode;
					
			}

		}



		// PRIVATE STATIC

		// INTERNAL
		/// <summary>
		/// The _parent multi track.
		/// </summary>
		internal MultiTrack _parentMultiTrack;
		
		
		//--------------------------------------
		//  Methods
		//--------------------------------------	
		// PUBLIC
		
		///<summary>
		///	 Constructor
		///</summary>
		public Track (AudioClip aAudioClip, float aMinimumIntensity_float, float aMaximumIntensity_float)
		{
			_audioClip = aAudioClip;
			//
			_minimumIntensity_float = aMinimumIntensity_float;
			_maximumIntensity_float = aMaximumIntensity_float;
			//
			_isPlaying_boolean = false;
			//
			_trackTransitionMode = TrackTransitionMode.NULL;

		}
		
		/// <summary>
		/// Deconstructor
		/// </summary>
		~Track ( )
		{
			
		}
		
		
		// PUBLIC

		

		
		
		// PUBLIC STATIC
		/// <summary>
		/// Froms the audio asset.
		/// </summary>
		/// <returns>The audio asset.</returns>
		/// <param name="aAudioClipName_string">A audio clip name_string.</param>
		public static Track fromAudioAsset (string aAudioClipName_string, float aMinimumIntensity_float, float aMaximumIntensity_float)
		{

			//
			AudioClip audioClip = Resources.Load (aAudioClipName_string) as AudioClip;
			if (audioClip == null) {

				throw new UnityException ("Audio Not Found For Path '"+ aAudioClipName_string+"'");
			}
			return new Track (audioClip, aMinimumIntensity_float, aMaximumIntensity_float);

		}



		// PRIVATE
		
		
		/// <summary>
		/// _dos the fade audio source from stop to play.
		/// </summary>
		/// <param name="aAudioSource">A audio source.</param>
		private void _doFadeAudioSourceFromStopToPlay ()
		{
			//
			AudioSource audioSource = _parentMultiTrack._getBestAudioSourceForTrack (this);
			
			//FADE SOUND FROM 0 TO 100% OVER X SECONDS
			//audioSource.volume = 0;
			Hashtable audioTo_hashtable 											= new Hashtable();
			audioTo_hashtable.Add(iT.AudioTo.volume,  								1);
			audioTo_hashtable.Add(iT.AudioTo.time,  								_parentMultiTrack.trackCrossFadeDuration);
			audioTo_hashtable.Add(iT.AudioTo.pitch,  								1);
			audioTo_hashtable.Add(iT.AudioTo.audiosource, 							audioSource);
			iTween.AudioTo (_parentMultiTrack._audioParentComponent.gameObject, 	audioTo_hashtable);

			//
			audioSource.Play();

		}
		
		/// <summary>
		/// _dos the fade audio source from play to stop.
		/// </summary>
		/// <param name="aAudioSource">A audio source.</param>
		private void _doFadeAudioSourceFromPlayToStop ()
		{
			//
			AudioSource audioSource = _parentMultiTrack._getBestAudioSourceForTrack (this);
			
			//FADE SOUND FROM 0 TO 100% OVER X SECONDS
			//aAudioSource.volume = 1;
			Hashtable audioTo_hashtable 												= new Hashtable();
			audioTo_hashtable.Add(iT.AudioTo.volume,  									0);
			audioTo_hashtable.Add(iT.AudioTo.time,  									_parentMultiTrack.trackCrossFadeDuration);
			audioTo_hashtable.Add(iT.AudioTo.pitch,  									1);
			audioTo_hashtable.Add(iT.AudioTo.audiosource, 								audioSource);
			iTween.AudioTo (_parentMultiTrack._audioParentComponent.gameObject, 		audioTo_hashtable);
			
			
		}

		
		// PRIVATE STATIC
		
		// PRIVATE COROUTINE
		
		// PRIVATE INVOKE

		// INTERNAL
		/// <summary>
		/// _dos the fade audio source from stop to play.
		/// </summary>
		/// <param name="aAudioSource">A audio source.</param>
		internal void doFadeAudioSourceFromStopToPlay ()
		{

			//START FADE
			_trackTransitionMode = TrackTransitionMode.FADE_TO_PLAY_BEGIN;

		}
		
		/// <summary>
		/// _dos the fade audio source from play to stop.
		/// </summary>
		/// <param name="aAudioSource">A audio source.</param>
		internal void doFadeAudioSourceFromPlayToStop ()
		{
			_trackTransitionMode = TrackTransitionMode.FADE_TO_STOP_BEGIN;
		}



		//--------------------------------------
		//  Events
		//--------------------------------------
		
		/// <summary>
		/// Ons the fade to play complete_ coroutine.
		/// </summary>
		/// <returns>The fade to play complete_ coroutine.</returns>
		IEnumerator  onFadeToPlayComplete_Coroutine ()
		{

			//WAIT
			yield return new WaitForSeconds (_parentMultiTrack.trackCrossFadeDuration);

			//DO SOMETHING
			_trackTransitionMode = TrackTransitionMode.FADE_TO_PLAY_COMPLETE;


			//RETURN
			yield return null;

		}


		/// <summary>
		/// Ons the fade to stop complete_ coroutine.
		/// </summary>
		/// <returns>The fade to stop complete_ coroutine.</returns>
		IEnumerator onFadeToStopComplete_Coroutine ()
		{
			
			
			//WAIT
			yield return new WaitForSeconds (_parentMultiTrack.trackCrossFadeDuration);
			
			//DO SOMETHING
			_trackTransitionMode = TrackTransitionMode.FADE_TO_STOP_COMPLETE;
			
			
			//RETURN
			yield return null;
		}

		/// <summary>
		/// Ons the fade to stop complete_ coroutine.
		/// </summary>
		/// <returns>The fade to stop complete_ coroutine.</returns>
		/// <param name="aAudioSource">A audio source.</param>
		/// <param name="aTrackCrossFadeDuration_float">A track cross fade duration_float.</param>
		IEnumerator onShowProgress ()
		{
			
			//Debug.Log ("m: " + __trackTransitionMode + " p: " + _isPlaying_boolean + " v: " +  _parentMultiTrack._getBestAudioSourceForTrack (this).volume.ToString ("00.0") );

			//WAIT
			yield return new WaitForSeconds (0.1f);

			//LOOP UNTIL PROGRESS NOT NEEDED
			if (_trackTransitionMode != TrackTransitionMode.NULL) {
				_parentMultiTrack._audioParentComponent.StartCoroutine (onShowProgress());
			}
			
		}
		
		
		
	}
}