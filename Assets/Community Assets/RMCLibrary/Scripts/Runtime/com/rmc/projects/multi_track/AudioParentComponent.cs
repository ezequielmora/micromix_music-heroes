/**
* Copyright (C) 2005-2014 by Rivello Multimedia Consulting (RMC).                    
* code [at] RivelloMultimediaConsulting [dot] com                                                  
*                                                                      
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the      
* "Software"), to deal in the Software without restriction, including  
* without limitation the rights to use, copy, modify, merge, publish,  
* distribute, sublicense, and#or sell copies of the Software, and to   
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:                                            
*                                                                      
* The above copyright notice and this permission notice shall be       
* included in all copies or substantial portions of the Software.      
*                                                                      
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR    
* OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.                                      
*/
// Marks the right margin of code *******************************************************************


//--------------------------------------
//  Imports
//--------------------------------------
using UnityEngine;

//--------------------------------------
//  Namespace
//--------------------------------------
using System.Collections;


namespace com.rmc.projects.multi_track
{
	
	//--------------------------------------
	//  Namespace Properties
	//--------------------------------------
	/// <summary>
	/// Coroutine method.
	/// </summary>
	public delegate IEnumerator CoroutineMethod();


	//--------------------------------------
	//  Class Attributes
	//--------------------------------------
	
	
	//--------------------------------------
	//  Class
	//--------------------------------------
	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * NOTE: This is indeed in use!
	 * 
	 * 		BUT MAINLY by non-monobehaviors who need to call audioParentComponent.StartCoroutine();
	 * 
	 * 		TODO: Consider ...
	 * 				1. adding some functionality in here...
	 * 				2. or moving this code into the other core classes and removing this class
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 **/
	public class AudioParentComponent : MonoBehaviour 
	{
		

		//--------------------------------------
		//  Properties
		//--------------------------------------
		
		// GETTER / SETTER
		
		// PUBLIC

		
		// PRIVATE STATIC
		
		
		//--------------------------------------
		//  Methods
		//--------------------------------------	
		// PUBLIC
		
		///<summary>
		///	 Constructor
		///</summary>
		public AudioParentComponent ()
		{
			
		}
		
		/// <summary>
		/// Deconstructor
		/// </summary>
		~AudioParentComponent ( )
		{
			
			
		}
		
		///<summary>
		///	Use this for initialization
		///</summary>
		void Start () 
		{
			//OPTIMIZATION: DON'T WORRY ABOUT GUI PHASE
			useGUILayout = false;
		}
		

		///<summary>
		///	Called once per frame
		/// 
		///</summary>
		void Update () 
		{
			
			
		}
		
		// PUBLIC

		/// <summary>
		/// Runs the coroutine.
		/// </summary>
		/// <returns>The coroutine.</returns>
		/// <param name="coroutineMethod">Coroutine method.</param>
		IEnumerator RunCoroutine(CoroutineMethod coroutineMethod)
		{
			return coroutineMethod();
		}

		/// <summary>
		/// Starts the coroutine delegate.
		/// </summary>
		/// <param name="coroutineMethod">Coroutine method.</param>
		public void StartCoroutineDelegate(CoroutineMethod coroutineMethod)
		{
			StartCoroutine("RunCoroutine", coroutineMethod);
		}
		
		// PUBLIC STATIC
		
		// PRIVATE
		
		// PRIVATE STATIC
		
		// PRIVATE COROUTINE
		
		// PRIVATE INVOKE
		
		//--------------------------------------
		//  Events
		//--------------------------------------
		
		
		
	}
}