﻿using UnityEngine;
using System;
using System.Collections;

namespace FutureCartographer.FCAudioTools.Demo
{
	public class AnimationClipEventReceive : MonoBehaviour
	{
		public AudioSource syncAudio;
		public Action<AnimationEvent> OnDidAudioMarkerEvent;
		public Action<AnimationEvent> OnDidAudioLoopEvent;

		void OnAudioMarkerEvent(AnimationEvent evt)
		{
			if (Mathf.Abs(syncAudio.time - evt.time) > 0.15f)
				return;

			Debug.Log(string.Format("OnAudioMarkerEvent: {0:0.000} {1} {2}", evt.time, evt.intParameter, evt.stringParameter));

			if (OnDidAudioMarkerEvent != null)
				OnDidAudioMarkerEvent(evt);
		}

		void OnAudioLoopEvent(AnimationEvent evt)
		{
			if (Mathf.Abs(syncAudio.time - evt.time) > 0.15f)
				return;

            Debug.Log(string.Format("OnAudioLoopEvent: {0:0.000} {1} {2}", evt.time, evt.intParameter, evt.stringParameter));

            if (evt.stringParameter == "loopStart")
			{
			}
			else if (evt.stringParameter == "loopEnd" && syncAudio.loop == true)

			if (OnDidAudioLoopEvent != null)
				OnDidAudioLoopEvent(evt);
		}
	}
}
