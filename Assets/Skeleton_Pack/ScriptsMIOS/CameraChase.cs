﻿using UnityEngine;
using System.Collections;

public class CameraChase : MonoBehaviour
{
	
	GameObject Player;
	public int xOffset, zOffset, yOffset;
	float xRotation, yRotation, zRotation;
	GameObject BlockObject;
	RaycastHit hit;
	Ray ray;
	Material oldShader;
	// Use this for initialization
	void Start()
	{
		
		xRotation = 47f;
		yRotation = 66f;
		zRotation = -7f;
		xOffset = -16;
		yOffset = 20;
		zOffset = -7;
		transform.Rotate(xRotation, yRotation, zRotation);
		Player = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update()
	{
		//ST// transform.position = new Vector3(Player.transform.position.x + xOffset, Player.transform.position.y + yOffset, Player.transform.position.z + zOffset);
		//RaycastHit[] hits;
		// you can also use CapsuleCastAll()
		// TODO: setup your layermask it improve performance and filter your hits.
		if (Physics.Raycast(transform.position, transform.forward, out hit, 50f))
		{
			if (hit.collider != Player.gameObject.GetComponent<Collider>())
			{
				Renderer R = hit.collider.GetComponent<Renderer>();
				// if (R == null)
				//    continue; // no renderer attached? go to next hit
				// TODO: maybe implement here a check for GOs that should not be affected like the player
				if (R != null)
				{
					AutoTransparent AT = R.GetComponent<AutoTransparent>();
					if (AT == null) // if no script is attached, attach one
					{
						AT = R.gameObject.AddComponent<AutoTransparent>();
					}
					AT.BeTransparent(); // get called every frame to reset the falloff
				}
				
			}
		}
	}
}