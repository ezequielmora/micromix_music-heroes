﻿var splashPlane : Transform;
 
private var fadeIn : boolean = false;
private var fadeOut : boolean = false;
 
 
 
function Start ()
{
    splashPlane.GetComponent.<Renderer>().material.color.a = 0;
   
    yield WaitForSeconds(1);
    fadeIn = true;
    yield WaitForSeconds(2);
    fadeIn = false;
    fadeOut = true;
   
}
 
function Update ()
{
    if(fadeIn == true)
    {
        splashPlane.GetComponent.<Renderer>().material.color.a += 0.01;
    }
    else if ( fadeOut == true )
    {
        splashPlane.GetComponent.<Renderer>().material.color.a -= 0.01;
       
        if( splashPlane.GetComponent.<Renderer>().material.color.a <= 0 )
        {
            Application.LoadLevel(1);
        }
       
    }
   
}