﻿using UnityEngine;
using System.Collections;

public class continousText : MonoBehaviour {
	

	private float speed = 5f;

	//private float leftSide;

	private float rightSide = Screen.width ;

	private GameObject textMoving;

	public Transform textMovingClone;

	void Start () {

		Instantiate(textMovingClone , transform.position, transform.rotation);

		textMoving = GameObject.FindGameObjectWithTag ("Moving");
	
	}
	
	void Update () {

		textMoving = GameObject.FindGameObjectWithTag ("Moving");
		
		textMoving.transform.Translate(Vector3.right * Time.deltaTime * speed, Space.World);

		Debug.Log (textMoving.transform.position.x);

		Debug.Log (Screen.width);
		
		if (textMoving.transform.position.x == rightSide) { 

			Destroy(textMoving);

			//Instantiate(textMovingClone , transform.position, transform.rotation);
		
		}
		
	}

}