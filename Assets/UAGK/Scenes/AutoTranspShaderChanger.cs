﻿using UnityEngine;
using System.Collections;

public class Highlight : MonoBehaviour
{
	private Shader startShader;
	private Shader customShader;
	
	void Start()
	{
		startShader = GetComponent<Renderer>().material.shader;
		customShader = (Shader)Resources.Load("Toon");
	}
	
	void ApplyHighlight(bool glow)
	{
		for(int i=0; i < GetComponent<Renderer>().materials.Length; i++)
		{
			if (glow)
			{
				GetComponent<Renderer>().materials[i].shader = customShader;
			}  
			else
			{
				GetComponent<Renderer>().materials[i].shader = startShader;
			}
		}
	}
	
	void OnMouseEnter()
	{
		ApplyHighlight(true);
	}
	
	void OnMouseExit()
	{
		ApplyHighlight(false);
	}
	
}
