﻿using UnityEngine;
using System.Collections;

public class CheckAndDestroyPlayer : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
        if(GameObject.FindGameObjectWithTag("MusicPlayer") != null){

            Destroy(GameObject.FindGameObjectWithTag("MusicPlayer"));
        
        }

        if (GameObject.FindGameObjectWithTag("SCPlayer") != null)
        {

            Destroy(GameObject.FindGameObjectWithTag("SCPlayer"));


            Destroy(GameObject.Find("SoundCloudService"));

            

        }


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
