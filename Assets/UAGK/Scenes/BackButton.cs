﻿using UnityEngine;
using System.Collections;

public class BackButton : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {


            Application.LoadLevel("intro");

            if (GameObject.FindGameObjectWithTag("Music") != null)
            {

                Destroy(GameObject.FindGameObjectWithTag("Music"));

            }


            if (GameObject.FindGameObjectWithTag("MAIN") != null)
            {

                Destroy(GameObject.FindGameObjectWithTag("MAIN"));

            }


            if (GameObject.FindGameObjectWithTag("ONLINE") != null)
            {

                Destroy(GameObject.FindGameObjectWithTag("ONLINE"));

            }
            


        }
    }

}
