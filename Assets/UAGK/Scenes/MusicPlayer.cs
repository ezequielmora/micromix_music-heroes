﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

    public OnlineManager activadorOnline;


    // Use this for initialization
    void Awake()
    {

        if (GameObject.Find("ONLINEMANAGER") != null) {

            activadorOnline = GameObject.Find("ONLINEMANAGER").GetComponent<OnlineManager>();

        }

        


    }


	// Use this for initialization
	void Start () {

        if (activadorOnline.OnlineMode == false && GameObject.FindGameObjectWithTag("MusicPlayer") == null) 
        
        {

            if (Application.loadedLevelName == "stage 6") { 
            
                GameObject instance = Instantiate(Resources.Load("Graveyard")) as GameObject; 
            
            }

            if (Application.loadedLevelName == "stage 4")
            {

                GameObject instance = Instantiate(Resources.Load("Asia")) as GameObject;

            }

            if (Application.loadedLevelName == "stage 3")
            {

                GameObject instance = Instantiate(Resources.Load("Forest")) as GameObject;

            }

            if (Application.loadedLevelName == "stage 5")
            {

                GameObject instance = Instantiate(Resources.Load("Egypt")) as GameObject;

            }


            if (Application.loadedLevelName == "stage 7")
            {

                GameObject instance = Instantiate(Resources.Load("City")) as GameObject;

            }

        } if (activadorOnline.OnlineMode == true && GameObject.FindGameObjectWithTag("SCPlayer") == null)
        
        
        {

            if (Application.loadedLevelName == "stage 6")
            {

                GameObject instance = Instantiate(Resources.Load("GraveyardSC")) as GameObject;

            }

            if (Application.loadedLevelName == "stage 4")
            {

                GameObject instance = Instantiate(Resources.Load("AsiaSC")) as GameObject;

            }

            if (Application.loadedLevelName == "stage 3")
            {

                GameObject instance = Instantiate(Resources.Load("ForestSC")) as GameObject;

            }

            if (Application.loadedLevelName == "stage 5")
            {

                GameObject instance = Instantiate(Resources.Load("EgyptSC")) as GameObject;

            }


            if (Application.loadedLevelName == "stage 7")
            {

                GameObject instance = Instantiate(Resources.Load("CitySC")) as GameObject;

            }

        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
