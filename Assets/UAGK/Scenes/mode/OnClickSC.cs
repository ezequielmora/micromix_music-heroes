﻿using UnityEngine;
using System.Collections;

public class OnClickSC : MonoBehaviour {

    public string levelSC;

    private GameObject destruir1;

    private GameObject destruir2;

    public OnlineManager activadorOnline;

    // Use this for initialization
    void Start()
    {

        destruir1 = GameObject.FindGameObjectWithTag("UIDestroy");

        destruir2 = GameObject.FindGameObjectWithTag("UIDestroy2");

        activadorOnline = GameObject.Find("ONLINEMANAGER").GetComponent<OnlineManager>();

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnClick() {


        Application.LoadLevel(levelSC);

        activadorOnline.OnlineMode = true;

        Destroy(destruir1);

        Destroy(destruir2);
    
    
    }
}
