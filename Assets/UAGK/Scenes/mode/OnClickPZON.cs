﻿using UnityEngine;
using System.Collections;

public class OnClickPZON : MonoBehaviour {

    public GameObject objetoAActivar;

    public GameObject objetoADesactivar;


    public OnlineManager cambioEstado;

    void Start()
    {

        cambioEstado = GameObject.Find("ONLINEMANAGER").GetComponent<OnlineManager>();

    }


	// Use this for initialization
	void Update () {

        objetoAActivar = GameObject.Find("ModeOFF");


        if (GameObject.Find("SC").activeInHierarchy == true)
        {

            objetoADesactivar = GameObject.Find("SC");

        }
	
	}
	
	
	


    void OnClick() {


            cambioEstado.OnlineMode = false;


            objetoADesactivar.SetActiveRecursively(false);

            objetoAActivar.SetActiveRecursively(true);
            


        
    
    }
}
