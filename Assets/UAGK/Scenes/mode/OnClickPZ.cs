﻿using UnityEngine;
using System.Collections;

public class OnClickPZ : MonoBehaviour {

    public string levelPZ;

    private GameObject destruir1;

    private GameObject destruir2;

	// Use this for initialization
	void Start () {

        destruir1 = GameObject.FindGameObjectWithTag("UIDestroy");

        destruir2 = GameObject.FindGameObjectWithTag("UIDestroy2");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnClick() {


        Application.LoadLevel(levelPZ);

        Destroy(destruir1);

        Destroy(destruir2);
    
    
    }
}
