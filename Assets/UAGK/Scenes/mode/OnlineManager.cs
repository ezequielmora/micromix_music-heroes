﻿using UnityEngine;
using System.Collections;

public class OnlineManager : MonoBehaviour {

    public bool OnlineMode = false;

    public bool checkON = false;

    public bool checkOFF = false;

    void Awake() {

        //OnlineMode = false;

        DontDestroyOnLoad(this.gameObject);
    
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (GameObject.Find("ModeON") != null && OnlineMode == true && checkON == false) {


            GameObject.Find("ModeON").SetActiveRecursively(true);

            checkON = true;
                
        
        }


        if (GameObject.Find("ModeOFF") != null && OnlineMode == false && checkOFF == false)
        {


            GameObject.Find("ModeOFF").SetActiveRecursively(true);

            checkOFF = true;


        }


        if (Application.loadedLevelName == "menu") {

            checkOFF = false;

            checkON = false;
        
        }

	
	}
}
