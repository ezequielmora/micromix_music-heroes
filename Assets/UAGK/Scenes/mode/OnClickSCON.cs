﻿using UnityEngine;
using System.Collections;

public class OnClickSCON : MonoBehaviour {

    public GameObject objetoAActivar;

    public GameObject objetoADesactivar;

    public OnlineManager cambioEstado;

    void Start() {

        cambioEstado = GameObject.Find("ONLINEMANAGER").GetComponent<OnlineManager>();
    
    }


	// Use this for initialization
	void Update () {

        objetoAActivar = GameObject.Find("ModeON");

        if (GameObject.Find("PZ").activeInHierarchy == true)
        {

            objetoADesactivar = GameObject.Find("PZ");

        }
	
	}
	


    void OnClick() {

        cambioEstado.OnlineMode = true;

        objetoADesactivar.SetActiveRecursively(false);

        objetoAActivar.SetActiveRecursively(true);
        
        
        

       
    
    }
}
