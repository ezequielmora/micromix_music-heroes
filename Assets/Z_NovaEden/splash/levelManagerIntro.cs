using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class levelManagerIntro : MonoBehaviour {


    public int waitTime;

	// Use this for initialization
	void Start () {


        StartCoroutine(Intro());
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public IEnumerator Intro() {


        yield return new WaitForSeconds(waitTime);


        //Application.LoadLevel("intro");

        //ST SceneManager.LoadScene("intro");

        // AEGIS FIX
        SimpleSceneFader.ChangeSceneWithFade("intro");

    }


}
