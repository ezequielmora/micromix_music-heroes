AssetStore_uMultTrack
===========================

The uMultiTrack package for Unity and C# allows you to create dynamic music for your games. Arrange 2 or more tracks into one 'song' and then adjust it based on an 'intensity' setting.


TODO LIST
===========================

* The project is complete
* See ’Todo:’ throughout codebase for theoretical todo items


RMC Credits
===========================

The music in this demo appears courtesy of composer Paulo Boher II and may be used royalty free for any use (preferably with previous notice to Paulo). Paulo is available for hire.

WEB:    http://www.paulobohrer.com
EMAIL:  paulobohrersound@gmail.com



RMC Disclaimer
===========================

Use this project, code, contents, ideas, dreams, etc… at your own risk and with no guarantees from us. The legal above each page of code speaks to the specific rights and any original owners/influencers of the code. 

Have fun learning with it!

Questions, comments? 
code@RivelloMultimediaConsulting.com



