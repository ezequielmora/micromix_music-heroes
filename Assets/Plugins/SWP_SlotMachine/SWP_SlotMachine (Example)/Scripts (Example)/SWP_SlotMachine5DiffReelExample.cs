//--------------ABOUT AND COPYRIGHT----------------------
// Copyright © 2013 SketchWork Productions Limited
//       support@sketchworkdev.com
//-------------------------------------------------------
using UnityEngine;
using System.Collections;

/// <summary>
/// This is the Slot machine example script file that just shows a simple GUI with some default logs when events are fired.
/// </summary>
public class SWP_SlotMachine5DiffReelExample: MonoBehaviour
{		
	void Start ()
	{
		GetComponent<SWP_SlotMachine>().OnReelStart += OnReelStart;
		GetComponent<SWP_SlotMachine>().OnEachReelFinished += OnEachReelFinished;
		GetComponent<SWP_SlotMachine>().OnReelsFinished += OnReelsFinished;
		GetComponent<SWP_SlotMachine>().OnMiddleMatch += OnMiddleMatch;
		GetComponent<SWP_SlotMachine>().OnTopMatch += OnTopMatch;
		GetComponent<SWP_SlotMachine>().OnBottomMatch += OnBottomMatch;
	}
	
	void OnGUI ()
	{
		GUI.Box(new Rect(10,10,100,120), "Spin Controls");
		
		if (GUI.Button(new Rect(20,40,80,20), "SPIN"))
		{
			GetComponent<SWP_SlotMachine>().StartReels();
		}

		if (GUI.Button(new Rect(20,70,80,20), "STOP"))
		{
			GetComponent<SWP_SlotMachine>().StopReels();
		}

		if (GUI.Button(new Rect(20,100,80,20), "SPIN/ST"))
		{
			GetComponent<SWP_SlotMachine>().StartStopReels();
		}
	}
	
	void OnReelStart(GameObject go)
	{
		//Debug.Log("(REEL START)");
	}
	
	void OnEachReelFinished(GameObject go, int _ReelNumber)
	{
		SWP_SlotMachine thisSlotMachine = GetComponent<SWP_SlotMachine>();
		//Debug.Log("(REEL FINISHED - " + thisSlotMachine.AllReels[_ReelNumber].GetReelItemName() + " [" + thisSlotMachine.AllReels[_ReelNumber].GetReelItemValue() + "]" + ")");

		if (thisSlotMachine.AllReels[_ReelNumber].GetMiddleReelItemValue() == 1)
			Debug.Log("You Win: " + thisSlotMachine.AllReels[_ReelNumber].GetMiddleReelItemName());
		else if (thisSlotMachine.AllReels[_ReelNumber].GetMiddleReelItemValue() > 0)
			Debug.Log("You Win: " + thisSlotMachine.AllReels[_ReelNumber].GetMiddleReelItemValue() + " " + thisSlotMachine.AllReels[_ReelNumber].GetMiddleReelItemName());
	}

	void OnReelsFinished(GameObject go)
	{
		//Debug.Log("(FINISHED ROLLING - " + GetComponent<SWP_SlotMachine>().DisplayMiddleReelValues() + ")");
	}
	
	void OnMiddleMatch(GameObject go)
	{
		//Debug.Log("(* * * * *  MIDDLE MATCH  * * * * *)");
	}
	
	void OnTopMatch(GameObject go)
	{
		//Debug.Log("(* * * * *  TOP MATCH  * * * * *) - " + GetComponent<SWP_SlotMachine>().DisplayTopReelValues() + ")");
	}
	
	void OnBottomMatch(GameObject go)
	{
		//Debug.Log("(* * * * *  BOTTOM MATCH  * * * * *) - " + GetComponent<SWP_SlotMachine>().DisplayBottomReelValues() + ")");
	}
}
