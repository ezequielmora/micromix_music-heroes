//--------------ABOUT AND COPYRIGHT----------------------
// Copyright © 2013 SketchWork Productions Limited
//       support@sketchworkdev.com
//-------------------------------------------------------
using UnityEngine;
using System.Collections;

/// <summary>
/// This is the Slot machine example script file that just shows a simple GUI with some default logs when events are fired.
/// </summary>
public class SWP_SlotMachine3SameReelExample: MonoBehaviour
{		
	void Start ()
	{
		GetComponent<SWP_SlotMachine>().OnReelStart += OnReelStart;
		GetComponent<SWP_SlotMachine>().OnEachReelFinished += OnEachReelFinished;
		GetComponent<SWP_SlotMachine>().OnReelsFinished += OnReelsFinished;
		GetComponent<SWP_SlotMachine>().OnMiddleMatch += OnMiddleMatch;
		GetComponent<SWP_SlotMachine>().OnTopMatch += OnTopMatch;
		GetComponent<SWP_SlotMachine>().OnBottomMatch += OnBottomMatch;
	}
	
	void OnGUI ()
	{
		GUI.Box(new Rect(10,10,100,180), "Spin Controls");
		
		if (GUI.Button(new Rect(20,40,80,20), "SPIN"))
		{
			GetComponent<SWP_SlotMachine>().StartReels();
		}

		if (GUI.Button(new Rect(20,70,80,20), "STOP"))
		{
			GetComponent<SWP_SlotMachine>().StopReels();
		}

		if (GUI.Button(new Rect(20,100,80,20), "NUDGE U"))
		{
			GetComponent<SWP_SlotMachine>().NudgeReels(true, 0);
		}

		if (GUI.Button(new Rect(20,130,80,20), "NUDGE D"))
		{
			GetComponent<SWP_SlotMachine>().NudgeReels(false, 0);
		}

		if (GUI.Button(new Rect(20,160,80,20), "SPIN/ST"))
		{
			GetComponent<SWP_SlotMachine>().StartStopReels();
		}
	}
	
	void OnReelStart(GameObject go)
	{
		Debug.Log("(REEL START)");
	}
	
	void OnEachReelFinished(GameObject go, int _ReelNumber)
	{
		SWP_SlotMachine thisSlotMachine = GetComponent<SWP_SlotMachine>();
		Debug.Log("(REEL FINISHED - " + thisSlotMachine.AllReels[_ReelNumber].GetMiddleReelItemName() + " [" + thisSlotMachine.AllReels[_ReelNumber].GetMiddleReelItemValue() + "]" + ")");
	}

	void OnReelsFinished(GameObject go)
	{
		Debug.Log("(FINISHED ROLLING - " + GetComponent<SWP_SlotMachine>().DisplayMiddleReelValues() + ")");

		//Check for diagonal match example logic
		SWP_SlotMachine thisSlotMachine = GetComponent<SWP_SlotMachine>();

		if ((thisSlotMachine.AllReels[0].GetTopReelItemValue() == thisSlotMachine.AllReels[1].GetMiddleReelItemValue() && thisSlotMachine.AllReels[1].GetMiddleReelItemValue() == thisSlotMachine.AllReels[2].GetBottomReelItemValue()))
			Debug.Log("(* * * * *  DIAGONAL DOWN (" + thisSlotMachine.AllReels[1].GetMiddleReelItemName() + ") MATCH  * * * * *)");
		else if ((thisSlotMachine.AllReels[0].GetBottomReelItemValue() == thisSlotMachine.AllReels[1].GetMiddleReelItemValue() && thisSlotMachine.AllReels[1].GetMiddleReelItemValue() == thisSlotMachine.AllReels[2].GetTopReelItemValue()))
			Debug.Log("(* * * * *  DIAGONAL UP (" + thisSlotMachine.AllReels[1].GetMiddleReelItemName() + ") MATCH  * * * * *)");
	}
	
	void OnMiddleMatch(GameObject go)
	{
		Debug.Log("(* * * * *  MIDDLE MATCH  * * * * *)");
	}
	
	void OnTopMatch(GameObject go)
	{
		Debug.Log("(* * * * *  TOP MATCH  * * * * *) - " + GetComponent<SWP_SlotMachine>().DisplayTopReelValues() + ")");
	}
	
	void OnBottomMatch(GameObject go)
	{
		Debug.Log("(* * * * *  BOTTOM MATCH  * * * * *) - " + GetComponent<SWP_SlotMachine>().DisplayBottomReelValues() + ")");
	}
}
