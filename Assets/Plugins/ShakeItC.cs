﻿using UnityEngine;
using System.Collections;

public class ShakeItC : MonoBehaviour {
	
	// Hierarchy DRAG E DROP over var GUI Text in Inspector
	public GUIText scoreTextX;

    public string reeplacedText = "MEZCLANDO TU NIVEL ...";


    public float accelerometerUpdateInterval = 1.0f / 60.0f;
	
	// The greater the value of LowPassKernelWidthInSeconds, the slower the filtered value will converge towards current input sample (and vice versa).
	public float lowPassKernelWidthInSeconds = 1.0f;
	
	// This next parameter is initialized to 2.0 per Apple's recommendation, or at least according to Brady! <img src="http://www.lucedigitale.com/blog/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
	public float shakeDetectionThreshold = 2.0f;
	
	private Vector3 lowPassValue= Vector3.zero;
	private Vector3 acceleration ;
	private Vector3 deltaAcceleration ;

	public float lowPassFilterFactor;
	
	private GameObject maquinita; 
	
	
	private bool checkReel = false;

    public bool MixAgain = false;

    public bool ReadyToStart1 = false;

    public bool ReadyToStart2 = false;

    public bool onClickLogo = false;
	
	private SWP_SlotMachine componenteMaquinita;

	private string SceneNameGame = "player select";
	
	void Awake(){
	
		lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;

		// this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
		//PhotonNetwork.automaticallySyncScene = true;
		
		
	}	


	void Start()
		
	{
		shakeDetectionThreshold *= shakeDetectionThreshold;
		lowPassValue = Input.acceleration;
		
		maquinita = GameObject.FindGameObjectWithTag("Machine");
		
		checkReel = false;
		
	}
	
	
	void Update()
	{
		
		acceleration = Input.acceleration;
		lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
		deltaAcceleration = acceleration - lowPassValue;


        componenteMaquinita = maquinita.GetComponent<SWP_SlotMachine>();



		if (deltaAcceleration.sqrMagnitude >= shakeDetectionThreshold && checkReel == false && MixAgain == false || Input.GetKey(KeyCode.Space) && checkReel == false && MixAgain == false || onClickLogo == true)
		{
			// Perform your "shaking actions" here, with suitable guards in the if check above, if necessary to not, to not fire again if they're already being performed.
			scoreTextX.text = reeplacedText;
			
			
			//componenteMaquinita = maquinita.GetComponent("SWP_SlotMachine");

			componenteMaquinita.StartStopReels();
			
			checkReel = true;

            onClickLogo = false;


            //ReadyToStart1 = true;

            // Testear Eze
            //ReadyToStart2 = true;
			
		}


        if (MixAgain == true && checkReel == true)
        {

            //ReadyToStart2 = true;

            //ReadyToStart1 = false;


            scoreTextX.text = "AMAZING! MIX AGAIN!!!";

            checkReel = false;

        }



        if (deltaAcceleration.sqrMagnitude >= shakeDetectionThreshold && checkReel == false && MixAgain == true || Input.GetKey(KeyCode.Space) && checkReel == false && MixAgain == true || onClickLogo == true)
        {
            // Perform your "shaking actions" here, with suitable guards in the if check above, if necessary to not, to not fire again if they're already being performed.
            scoreTextX.text = "MIXING YOUR LEVEL AGAIN...";


            //componenteMaquinita = maquinita.GetComponent("SWP_SlotMachine");




            componenteMaquinita.StartStopReels();

            checkReel = true;

            onClickLogo = false;


            //ReadyToStart2 = false;


            //ST
            MixAgain = false;

        }        
		
	}

}