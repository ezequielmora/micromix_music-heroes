using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (WaveForm1))]
public class WaveForm1Inspector : Editor {

	WaveForm1 contextCast;
	
	void OnEnable () {	
			
		contextCast = target as WaveForm1;
		
	}
	
	public override void OnInspectorGUI () {
		
		serializedObject.Update ();
		EditorGUILayout.Space ();
		
		EditorGUI.BeginChangeCheck ();
		
		contextCast.sample1ScalarGlobal = EditorGUILayout.Slider ("sample1 scalar:", contextCast.sample1ScalarGlobal, 0.0f, contextCast.sample1ScalarGlobalMax);
		
		if (EditorGUI.EndChangeCheck ()) {	

			serializedObject.Update();
		}

		EditorGUILayout.Space ();

		EditorGUI.BeginChangeCheck ();
		
		contextCast.sample2ScalarGlobal = EditorGUILayout.Slider ("sample2 scalar:", contextCast.sample2ScalarGlobal, 0.0f, contextCast.sample2ScalarGlobalMax);
		
		if (EditorGUI.EndChangeCheck ()) {	
			
			serializedObject.Update();
		}
		EditorGUILayout.Space ();
		EditorGUILayout.Space ();
		base.OnInspectorGUI ();
		
	}
}
