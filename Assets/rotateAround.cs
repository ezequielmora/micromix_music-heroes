﻿using UnityEngine;
using System.Collections;

public class rotateAround : MonoBehaviour {
	void Update() {
		transform.RotateAround(Vector3.zero, Vector3.forward * -1, 50 * Time.deltaTime);
	}
}