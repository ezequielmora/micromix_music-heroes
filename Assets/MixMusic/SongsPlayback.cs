﻿using UnityEngine;
using System.Collections;

public class SongsPlayback : MonoBehaviour {

    public AudioSource LargerGraveyard;

    void Awake() {

        //DontDestroyOnLoad (this.gameObject);

    }


	// Use this for initialization
	void Start () {

        LargerGraveyard.GetComponent<AudioSource>().loop = true;

        LargerGraveyard.GetComponent<AudioSource>().Play();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
